# Millipede

A thought experiment for organising micro-services in a single (mono) repository but still allowing for services to run in their own containers if necessary.